(function() {
  'use strict';

  angular
    .module('7ClassesRevamp')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider,$locationProvider) {
       $stateProvider
      .state('home', {
        url: '/',
        views: {
          '@': {
            templateUrl: 'app/home/home.html',
            controller: 'HomePageController'
          },
          'main-content@home': {
            templateUrl: 'app/components/main-page/main-page.html',
            controller:   'MainPageController'
          },
          'header@home': {
            templateUrl: 'app/components/header/header.html',
            controller: 'HeaderController'
          },
          'footer@home': {
            templateUrl: 'app/components/footer/footer.html',
            controller: 'FooterController'
          }
        }
      })
   $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });
  }
})();
