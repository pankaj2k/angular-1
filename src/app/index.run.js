(function() {
  'use strict';

  angular
    .module('7ClassesRevamp')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
